import argparse
import joblib
import json
import os
import pandas as pd
from typing import Text, List, Dict, Tuple
import yaml
import itertools
import matplotlib.colors
import matplotlib.pyplot as plt
import numpy as np

from sklearn.base import BaseEstimator
from sklearn.metrics import confusion_matrix, f1_score
from sklearn.datasets import load_iris

def get_target_names() -> List:
    return load_iris(as_frame=True).target_names.tolist()


def evaluate(df: pd.DataFrame, target_column: Text, clf: BaseEstimator) -> Dict:
    """Evaluate classifier on dataset

    Args:
        df {pandas.DataFrame}: dataset
        target_column {Text}: target column name
        clf {sklearn.base.BaseEstimator}: classifier (trained model)

    Returns:
        Tuple[float, numpy.ndarray]: tuple of score and confusion matrix

    """

    # Get X and Y
    y_test = df.loc[:, target_column].values.astype('int32')
    X_test = df.drop(target_column, axis=1).values.astype('float32')

    prediction = clf.predict(X_test)
    f1 = f1_score(y_true=y_test, y_pred=prediction, average='macro')
    cm = confusion_matrix(prediction, y_test)

    return {
        'f1': f1,
        'cm': cm,
        'actual': y_test,
        'predicted': prediction
    }


def plot_confusion_matrix(cm: np.array,
                          target_names: List[Text],
                          title: Text = 'Confusion matrix',
                          cmap: matplotlib.colors.LinearSegmentedColormap = None,
                          normalize: bool = True):
    """
    given a sklearn confusion matrix (cm), make a nice plot

    Arguments
    ---------
    cm:           confusion matrix from sklearn.metrics.confusion_matrix

    target_names: given classification classes such as [0, 1, 2]
                  the class names, for example: ['high', 'medium', 'low']

    title:        the text to display at the top of the matrix

    cmap:         the gradient of the values displayed from matplotlib.pyplot.cm
                  see http://matplotlib.org/examples/color/colormaps_reference.html
                  plt.get_cmap('jet') or plt.cm.Blues

    normalize:    If False, plot the raw numbers
                  If True, plot the proportions

    Usage
    -----
    plot_confusion_matrix(cm           = cm,                  # confusion matrix created by
                                                              # sklearn.metrics.confusion_matrix
                          normalize    = True,                # show proportions
                          target_names = y_labels_vals,       # list of names of the classes
                          title        = best_estimator_name) # title of graph

    Citiation
    ---------
    http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html

    """

    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))

    return plt


def evaluate_model(config_path: Text) -> None:
    """Evaluate model.
    Args:
        config_path {Text}: path to config
    """

    config = yaml.safe_load(open(config_path))
    target_column = config['featurize']['target_column']
    model_name = config['base']['model']['model_name']
    models_folder = config['base']['model']['models_folder']
    experiment_folder = config['base']['experiments']['experiments_folder']

    test_df = pd.read_csv(config['data_split']['test_path'])

    model = joblib.load(os.path.join(models_folder, model_name))
    report = evaluate(df = test_df,
                      target_column = target_column,
                      clf = model)
    classes = get_target_names()

    # save f1 metrics file
    metrics_path = os.path.join(experiment_folder, config['evaluate']['metrics_file'])
    json.dump(
        obj={'f1_score': report['f1']},
        fp=open(metrics_path, 'w')
    )
    print(f'F1 metrics file saved to : {metrics_path}')

    # save confusion_matrix.png
    plt = plot_confusion_matrix(cm = report['cm'],
                                target_names = get_target_names(),
                                normalize=False)
    confusion_matrix_png_path = os.path.join(experiment_folder, config['evaluate']['confusion_matrix_png'])
    plt.savefig(confusion_matrix_png_path)
    print(f'Confusion matrix saved to : {confusion_matrix_png_path}')

    # save confusion_matrix.json
    classes_path = os.path.join(experiment_folder, config['evaluate']['classes_path'])
    mapping = {
        0: classes[0],
        1: classes[1],
        2: classes[2]
    }
    df = (pd.DataFrame({'actual': report['actual'],
                        'predicted': report['predicted']})
          .assign(actual=lambda x: x.actual.map(mapping))
          .assign(predicted=lambda x: x.predicted.map(mapping))
          )
    df.to_csv(classes_path, index=False)
    print(f'Classes actual/predicted saved to : {classes_path}')


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    evaluate_model(config_path=args.config)
