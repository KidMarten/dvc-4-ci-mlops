import argparse
import pandas as pd
from typing import Text
import yaml


def extract_features(df: pd.DataFrame) -> pd.DataFrame:
    """Extract features.

    Args:
        df {pandas.DataFrame}: dataset

    Returns:
        pandas.DataFrame: updated dataset with new features
    """

    dataset = df.copy()
    dataset['sepal_length_to_sepal_width'] = dataset['sepal_length'] / dataset['sepal_width']
    dataset['petal_length_to_petal_width'] = dataset['petal_length'] / dataset['petal_width']

    dataset = dataset[[
        'sepal_length', 'sepal_width', 'petal_length', 'petal_width',
        'sepal_length_to_sepal_width', 'petal_length_to_petal_width',

        'target'
    ]]

    return dataset


def featurize(config_path: Text) -> None:
    """Create new features.
    Args:
        config_path {Text}: path to config
    """

    config = yaml.safe_load(open(config_path))

    dataset = pd.read_csv(config['data_load']['raw_data_path'])
    featured_dataset = extract_features(dataset)

    features_path = config['featurize']['features_path']
    featured_dataset.to_csv(features_path, index=False)


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    featurize(config_path=args.config)