import argparse
from typing import Text, List
import yaml
import pandas as pd
from sklearn.datasets import load_iris


def get_dataset() -> pd.DataFrame:
    """Read dataset into pandas.DataFrame
    Returns:
        pandas.DataFrame
    """

    data = load_iris(as_frame=True)

    dataset = data.frame
    dataset.rename(
        columns=lambda colname: colname.strip(' (cm)').replace(' ', '_'),
        inplace=True
    )

    return dataset


def get_target_names() -> List:
    return load_iris(as_frame=True).target_names.tolist()


def data_load(config_path: Text) -> None:
    """Load raw data.
    Args:
        config_path {Text}: path to config
    """

    config = yaml.safe_load(open(config_path))
    dataset = get_dataset()
    dataset.to_csv(config['data_load']['raw_data_path'], index=False)


if __name__ == '__main__':

    args_parser = argparse.ArgumentParser()
    args_parser.add_argument('--config', dest='config', required=True)
    args = args_parser.parse_args()

    data_load(config_path=args.config)

